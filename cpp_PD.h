/*
 * PD.h
 *
 *  Created on: Dec 13, 2013
 *      Author: dellu
 *      Programmazione dinamica funzionante (test 13/12/2013)
 */


#ifndef PD_H_
#define PD_H_

#include <stdio.h>
#include <iostream>
#include <string.h>
#include <fstream>
#include <math.h>
#include <stdlib.h>

struct nodo{
	int value;
};

class PD {
	nodo **m_nodi;
	double **m_nodeCost;
	int m_numStati;
	int m_numPassi;
	bool m_vincolata;
	int m_vincolo;
	int **m_path;
	double ***m_arcCost;
	int *m_idealPath;
	bool m_debug;

	double getCostTest(int indexStato, int indexNextStato, int indexPasso);

public:
	PD();
	virtual ~PD();

	void init(int passi, int stati, bool b, int v, bool d);
	void setCost(double ***cost);
	int* findPath();
};

#endif /* PD_H_ */
