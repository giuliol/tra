/*
 * cpp_setCostQoE.cpp
 *
 *  Created on: Aug 28, 2014
 *      Author: scnl_user
 */

#include "tra.h"

//#include <math.h>
#include <cmath>

#include "constants.h"

using namespace std;

TrAllocator::TrAllocator() {
	// TODO Auto-generated constructor stub

}

TrAllocator::~TrAllocator() {

}

/*
 * Inizializzo SetCostQoE(numero di esecuzioni della programmazione dinamica)
 * Nota: devo sapare il numero di esecuzioni della PD per istanziare e assegnare i valori alla
 * variabile m_cr (rain ratio).
 */
void TrAllocator::init(int s, int r, int n, double *ploss, int mauSize, double * sizes_lut, int * fec_lut, double * ssims_lut) {
	m_numStations = s;
	m_Rtot = r;
	m_norm = n;

	m_mauSize = mauSize;

	m_numMau=m_Rtot/m_mauSize;

	m_ploss = new double[m_numStations];
	m_ploss = ploss;

	m_sizes_lut = sizes_lut;
	m_fec_lut = fec_lut;
	m_ssims_lut = ssims_lut;

}

/*
 * Setto la matrice con i costi di tutti gli archi ad ogni esecuzione della PD.
 * Nota indexInteral mi serve per selezionare il valore corretto di m_cr
 */
void TrAllocator::setCostsPd(){
	double Qi;
	double Ri;
	double Wir;
	double Wiq;
	double sumW=0;

	m_pd.init(m_numStations+1, m_numMau+1, true, m_numMau, false);

	m_cost = new  double**[m_numStations];

	for(int indexS=0;  indexS<m_numStations; indexS++){
		sumW = sumW + 1/ cri_PL(m_ploss[indexS]);
		m_cost[indexS]=new double*[m_numMau+1];
		for(int indexUma=0;  indexUma<m_numMau+1; indexUma++){
			m_cost[indexS][indexUma]=new double[m_numMau+1];
		}
	}

	m_allocated_bandwidth = new int[m_numStations];




	double m_sumCr=0;

	for(int indexStazione=0; indexStazione<m_numStations; indexStazione++){
		m_sumCr += cri_PL(m_ploss[indexStazione]);
	}
	for(int indexStazione=0; indexStazione<m_numStations; indexStazione++){
		m_idealQ = Q_r_pl(m_Rtot, m_ploss[indexStazione]);
		cout<<"calcolo idealQ =" << m_idealQ<<endl;
		for(int indexStato=0; indexStato<m_numMau+1; indexStato++){

			for(int indexNextStato=0; indexNextStato<m_numMau+1; indexNextStato++){


				//m_idealQ = 100;
				//printf("indexStato=%d, indexNextStato=%d\n",indexStato,indexNextStato);

				// Impongo che il grafo sia solo crescente.
				if(indexStato>indexNextStato)
					m_cost[indexStazione][indexStato][indexNextStato]=1000000000;
				else {

					//  pesi prima versione

/*
					Wir = cri_PL(m_ploss[indexStazione]) / m_numStations;
					Wiq = (1-cri_PL(m_ploss[indexStazione])) / m_numStations;
					/*/

					// pesi unitari
					Wir = 0;
					Wiq = 1;

					//*/
					/*
					// pesi solo su Ri
					Wir = (m_crInUse[indexStazione] / m_sumCr) /m_numStations;
					Wiq = 1.0 / m_numStations;
					 */

					// pesi solo su Qi
					/*
					Wir = 1.0 / m_numStations;
					Wiq = (1.0 - m_crInUse[indexStazione] / (m_numStations - m_sumCr) )/m_numStations;
					 */

					// pesi solo su Qi e 0 su Ri
					/*
					Wir = 0.0 / m_numStations;
					Wiq = (1.0 - m_crInUse[indexStazione] / (m_numStations - m_sumCr) );
					 */

					Ri = ((indexNextStato-indexStato) * m_mauSize);
					Qi = Q_r_pl( Ri, m_ploss[indexStazione] );

					 cout<< Qi <<", ideal Q="<< m_idealQ<<endl;

					// Q OPTIMIZATION ******************************************
					m_cost[indexStazione][indexStato][indexNextStato] =
						 Wiq * pow(((int)Qi - (int)m_idealQ)/100.0,m_norm) + Wir * pow(( Ri/ (double)m_Rtot),m_norm );

					// SSIM OPTIMIZATION ****************************************
					//m_cost[indexStazione][indexStato][indexNextStato] = pow((1.0 - ssim_Q(Qi)),2) ; //+ Wir * pow(( Ri/ m_Rtot),m_norm ) ;

				}
			}
		}
	}

	m_pd.setCost(m_cost);

}

double TrAllocator::ssim_Q(double Qi){

	double lower_ssim = m_ssims_lut[(int)floor(Qi)];
	double upper_ssim = m_ssims_lut[(int)ceil(Qi)];
	double delta = upper_ssim - lower_ssim;
	double remainder = Qi - floor(Qi);
	return lower_ssim + remainder*delta;
}

double TrAllocator::Q_r_pl(double r, double pl){
	//trova CW/s
	double cw_sec = r / (8 * CWLEN * (PKTSIZE + HEADERSIZE + RQHEADERSIZE) );
	double target_frames = (double) FPS / (double)cw_sec;
	int effective_payload = NET_PAYLOAD * (CWLEN - fec_PL(pl));

	double per_frame = effective_payload / target_frames;
	//cout << "Per_frame "<<per_frame<<endl;
	int i;
	for(i = 1; i<100 ; i++){
		if(per_frame < m_sizes_lut[i]){
			double remainder = per_frame - m_sizes_lut[i-1];
			double difference  = m_sizes_lut[i] - m_sizes_lut[i-1];
			double rem_percentage = remainder / difference;
			if(difference != 0)
				return (i - 1) + rem_percentage ;
			else return (i - 1);
		}
	}
	return 99;
}

double TrAllocator::cri_PL(double pl){

	int fec =  fec_PL(pl);
	return (double)((CWLEN - (double)fec)/(CWLEN));

}


int TrAllocator::fec_PL(double pl){
	int index = ceil(pl * 100);
	index = min(index,100);
	index = max(index,0);
	return m_fec_lut[index];
}

void TrAllocator::cleanUp(){
	delete [] m_allocated_bandwidth;

	for(int indexS=0;  indexS<m_numStations; indexS++){
		for(int indexUma=0;  indexUma<m_numMau+1; indexUma++){
			delete [] m_cost[indexS][indexUma];
		}
		delete [] m_cost[indexS];
	}

}

int TrAllocator::getNumStations(){
	return m_numStations;
}

/*
 * Print the allocation obtained with TRA algorithm
 */

void TrAllocator::printAllocationTra(){
	m_allocated_bandwidth=m_pd.findPath();
	int somma=0;
	double Ri;
	for(int indexStation=0; indexStation<m_numStations; indexStation++){
		if(indexStation>0){
			somma = m_allocated_bandwidth[indexStation-1];
		}

		Ri = (m_allocated_bandwidth[indexStation]-somma)*m_mauSize;
		cout<< Ri << ", est.ssim="<< ssim_Q(floor(Q_r_pl(Ri,m_ploss[indexStation]))) << ", dummy would be "<<ssim_Q(floor(Q_r_pl((double)m_Rtot/m_numStations,m_ploss[indexStation]))) <<endl ;
		//cout<< Ri<<endl;

	}
}
