/*
 * cpp_setCostQoE.h
 *
 *  Created on: Aug 28, 2014
 *      Author: scnl_user
 */

#ifndef ALLOCATOR_H_
#define ALLOCATOR_H_

#include "cpp_PD.h"

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sstream>


class TrAllocator {

	PD m_pd;

	double ***m_cost;						// Cost associated to each arch

	int m_numStations;						// 2 Number of stations
	int m_mauSize; 							// 3 MAU size
	int m_Rtot; 							// 4 Overall available transmission rate (Rtot) [Hz]
	int m_norm;								// 11 Adopted norm
	int m_numMau;				 			// Number of MAUs

	int *m_allocated_bandwidth;				// Number of allocated MAUs (PD output)

	double m_idealQ;						// utopia point

	double *m_ploss;

	double * m_sizes_lut;
	int * m_fec_lut;
	double * m_ssims_lut;

public:
	TrAllocator();

	virtual ~TrAllocator();

	void init(int s, int r, int n, double *per,int mauSize, double * sizes_lut, int * fec_lut, double * ssims_lut);

	int getQ_r(double frame_size);

	void cleanUp();

	void setCostsPd();

	void printAllocationTra();

	double ssim_Q(double Qi);

	double Q_r_pl(double r, double pl);

	double cri_PL(double pl);

	int fec_PL(double pl);

	int getNumStations();
};

#endif
