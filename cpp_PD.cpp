/*
 * PD.cpp
 *
 *  Created on: Dec 13, 2013
 *      Author: dellu
 */

#include "cpp_PD.h"

using namespace std;

PD::PD() {
	// TODO Auto-generated constructor stub

}

PD::~PD() {
	// TODO Auto-generated destructor stub

}

/*
 * Inizializzo la programmazione dinamica:
 * int passi, int stati, bool b (true = vincolata), int v (vincolo),
 * bool d  (true = debug attivo)
 */
void PD::init(int passi, int stati, bool b, int v, bool d){
	m_vincolata=b;
	m_vincolo=v;
	m_debug=d;
	m_numPassi=passi;
	m_numStati=stati;
	m_nodeCost = new double*[m_numPassi];
	m_nodi = new nodo*[m_numPassi];
	m_path = new int*[m_numPassi];
	m_idealPath = new int[m_numPassi];
	m_arcCost = new  double**[m_numPassi];
	for(int indexP=0;  indexP<m_numPassi; indexP++){
		m_arcCost[indexP]=new double*[m_numStati];
		for(int indexStato=0; indexStato<m_numStati; indexStato++){
			m_arcCost[indexP][indexStato]=new double[m_numStati];
		}
	}
}

/*
 * Setto il costo dei nodi e degli archi
 * double costo degli archi
 */
void PD::setCost(double ***cost){
	m_arcCost=cost;
	for(int indexPasso=0; indexPasso<m_numPassi; indexPasso++){
		m_nodeCost[indexPasso]=new double[m_numStati];
		m_nodi[indexPasso]=new nodo[m_numStati];
		m_path[indexPasso]=new int[m_numStati];
	}
	for(int indexPasso=0; indexPasso<m_numPassi; indexPasso++){
		for(int indexStato=0; indexStato<m_numStati; indexStato++){
			m_nodeCost[indexPasso][indexStato]=10000000000000000;
			if((!m_vincolata)&&(indexPasso==m_numPassi-1)){
				m_nodeCost[indexPasso][indexStato]=0;
			}if((m_vincolata)&&(indexPasso==m_numPassi-1)){
				m_nodeCost[indexPasso][m_vincolo]=0;
			}
		}
	}
}

/*
 * Calcolo il percorso ottimo
 */
int* PD::findPath(){
	double tempCost;
	for(int indexPasso=m_numPassi-2; indexPasso>0; indexPasso--){
		if(m_debug)cout<<"-> Passo: "<<indexPasso<<"\n";
		for(int indexStato=0; indexStato<m_numStati; indexStato++){
			if(m_debug)cout<<"\t-> Stato: "<<indexStato<<"\n";
			for(int indexNextStato=indexStato; indexNextStato<m_numStati; indexNextStato++){
				if(m_debug)cout<<"\t\t-> NextStato: "<<indexNextStato<<"\n";
				//m_arcCost[indexPasso][indexStato][indexNextStato]=getCostTest(indexStato,indexNextStato,indexPasso);
				tempCost=m_nodeCost[indexPasso+1][indexNextStato]+m_arcCost[indexPasso][indexStato][indexNextStato];
				if(m_debug){
					cout<<"\t\t\tm_arcCost["<<indexPasso<<"]["<<indexStato<<"]["<<indexNextStato<<"]: "<<m_arcCost[indexPasso][indexStato][indexNextStato]<<"\n";
					cout<<"\t\t\tm_nodeCost["<<indexPasso+1<<"]["<<indexNextStato<<"]: "<<m_nodeCost[indexPasso+1][indexNextStato]<<"\n";
					cout<<"\t\t\tm_nodeCost["<<indexPasso<<"]["<<indexNextStato<<"]: "<<m_nodeCost[indexPasso][indexNextStato]<<"\n";
					cout<<"\t\t\ttempCost: "<<tempCost<<"\n";
				}
				if(tempCost < m_nodeCost[indexPasso][indexStato]){
					m_nodeCost[indexPasso][indexStato]=tempCost;
					m_path[indexPasso][indexStato]=indexNextStato;
					if(m_debug){
						cout<<"\t\t\tAGGIORNO:\n";
						cout<<"\t\t\tm_nodeCost["<<indexPasso<<"]["<<indexStato<<"]: "<<tempCost<<"\n";
						cout<<"\t\t\tm_path["<<indexPasso<<"]["<<indexStato<<"]: "<<indexNextStato<<"\n";
					}
				}
			}
		}
	}
	if(m_debug)cout<<"-> Passo: 0\n";
	if(m_debug)cout<<"\t-> Stato: 0\n";
	for(int indexNextStato=0; indexNextStato<m_numStati; indexNextStato++){
		//m_arcCost[indexPasso][indexStato][indexNextStato]=getCostTest(indexStato,indexNextStato,indexPasso);
		if(m_debug)cout<<"\t\t-> NextStato: "<<indexNextStato<<"\n";
		tempCost=m_nodeCost[1][indexNextStato]+m_arcCost[0][0][indexNextStato];
		if(m_debug){
			cout<<"\t\t\tm_arcCost["<<0<<"]["<<0<<"]["<<indexNextStato<<"]: "<<m_arcCost[0][0][indexNextStato]<<"\n";
			cout<<"\t\t\tm_nodeCost["<<1<<"]["<<indexNextStato<<"]: "<<m_nodeCost[1][indexNextStato]<<"\n";
			cout<<"\t\t\ttempCost: "<<tempCost<<"\n";
			cout<<"\t\t\tm_nodeCost["<<0<<"]["<<0<<"]: "<<m_nodeCost[0][0]<<"\n";
		}
		if(tempCost < m_nodeCost[0][0]){
			m_nodeCost[0][0]=tempCost;
			m_path[0][0]=indexNextStato;
			if(m_debug){
				cout<<"\t\t\tAGGIORNO:\n";
				cout<<"\t\t\tm_nodeCost["<<0<<"]["<<0<<"]: "<<tempCost<<"\n";
				cout<<"\t\t\tm_path["<<0<<"]["<<0<<"]: "<<indexNextStato<<"\n";
			}
		}
	}
	m_idealPath[0]=m_path[0][0];
	int temp = m_path[0][0];
	for(int indexPasso=1; indexPasso<m_numPassi; indexPasso++){
		m_idealPath[indexPasso]=m_path[indexPasso][temp];
		temp=m_path[indexPasso][temp];
		if(m_debug)cout<<m_idealPath[indexPasso-1]<<"\n";
	}

	return m_idealPath;
}


/*
 * Ottengo il costo associato all'arco identificato da: int indexStato,
 * int indexNextStato, int indexPasso (Funzione utilizzata per test)
 */
double PD::getCostTest(int indexStato, int indexNextStato, int indexPasso){
	double*** cost;
	cost = new  double**[m_numPassi];
	for(int indexP=0;  indexP<m_numPassi; indexP++){
		cost[indexP]=new double*[m_numStati];
		for(int indexStato=0; indexStato<m_numStati; indexStato++){
			cost[indexP][indexStato]=new double[m_numStati];
		}
	}
	cost[0][0][0]=2;
	cost[0][0][1]=10;
	cost[0][0][2]=11;
	cost[0][1][0]=100;
	cost[0][1][1]=100;
	cost[0][1][2]=100;
	cost[0][2][0]=100;
	cost[0][2][1]=100;
	cost[0][2][2]=100;
	cost[1][0][0]=1;
	cost[1][0][1]=10;
	cost[1][0][2]=11;
	cost[1][1][0]=100;
	cost[1][1][1]=12;
	cost[1][1][2]=13;
	cost[1][2][0]=100;
	cost[1][2][1]=100;
	cost[1][2][2]=12;
	cost[2][0][0]=13;
	cost[2][0][1]=11;
	cost[2][0][2]=1;
	cost[2][1][0]=100;
	cost[2][1][1]=30;
	cost[2][1][2]=11;
	cost[2][2][0]=100;
	cost[2][2][1]=100;
	cost[2][2][2]=10;
	return cost[indexPasso][indexStato][indexNextStato];
}




